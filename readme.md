## Aws Secrets Manager

This package allows you to retrieve secrets from AWS secrets.


Once installed you can do stuff like this:

```php
// Getting a secret
credentials('SENDGRID_API_KEY', 'default_key');
credentials()->get('SENDGRID_API_KEY', 'default_key')


//If a specified key is missing in all the secrets manager   
//We default to the environment variables, and then return whatever default was passed in if noth
```

The following environment variables are assumed/required and are currently not yet configurable.
```
//We can specify multiple Secret namespaces by separating them with a space
//when multiple keys exist in the same namespace, preference is given to the namespace on the right
AWS_SSM_SECRET_NAMESPACES="SCHOOLABLE/PROD SCHOOLABLE/PROD/APP_NAME"
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY

```

