<?php
namespace SecretsManager\Credentials;

use Aws\Exception\AwsException;
use Aws\SecretsManager\SecretsManagerClient;
use Illuminate\Support\Str;

class Credentials
{
    const AWS_SECRET_ID = 'SecretId';
    const AWS_SECRET_STRING = 'SecretString';
    const CACHE_PREFIX = 'AWS_SSM_CREDENTIALS';
    private static $instance;

    public static function getInstance()
    {
        if (self::$instance == null)
        {
            self::$instance = new Credentials();
        }

        return self::$instance;
    }

    public function get($key, $default = null)
    {
        $secretNamespaces = explode(' ', env('AWS_SSM_SECRET_NAMESPACES'));

        return collect($secretNamespaces)->filter()->map(function ($secretNamespace){
            return $this->resolve($secretNamespace);
        })->collapse()
        ->get($key, env($key, $default ));
    }

    public function getCacheKey($secret)
    {
        return Str::slug(
            static::CACHE_PREFIX.'-'.$secret
        );
    }

    public function resolve($secretNamespace)
    {
        $secretArray = $this->getCache($secretNamespace);

        if( ! is_null($secretArray)) {
            return $secretArray;
        }

        try {
            $secretsManagerClient = new SecretsManagerClient([
                'region' => env('AWS_DEFAULT_REGION'),
                'version' => 'latest',
                'credentials' => new \Aws\Credentials\Credentials(env('AWS_ACCESS_KEY_ID'), env('AWS_SECRET_ACCESS_KEY')),
            ]);
            $response = $secretsManagerClient->getSecretValue([self::AWS_SECRET_ID => $secretNamespace])->get(self::AWS_SECRET_STRING);
            $secretArray = json_decode($response, true);
            $this->putCache($secretNamespace, $secretArray);
            return $secretArray;
        } catch (AwsException $e){
            $this->putCache($secretNamespace, []);
            return [];
        }
    }

    private function putCache($secretNamespace, $secretArray) {
        $cacheKey = $this->getCacheKey($secretNamespace);
        $configPath = $this->getCachedSecretPath($cacheKey);
        file_put_contents($configPath, '<?php return '.var_export($secretArray, true).';'.PHP_EOL,  0);
    }

    private function getCache($secretNamespace)
    {
        $cacheKey = $this->getCacheKey($secretNamespace);
        if (file_exists($cached = $this->getCachedSecretPath($cacheKey))) {
            return ( require $cached );
        }
        return null;
    }

    public function getCachedSecretPath(string $cacheKey)
    {
        return base_path("bootstrap/cache/$cacheKey.php");
    }

}