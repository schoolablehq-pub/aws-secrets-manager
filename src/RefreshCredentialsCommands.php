<?php


namespace SecretsManager\Credentials;

use Illuminate\Console\Command;

class RefreshCredentialsCommands extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'credentials:refresh';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh existing credentials.';
    /**
     * The command handler.
     *
     * @return void
     */
    public function handle()
    {
        $secretNamespaces = explode(' ', env('AWS_SSM_SECRET_NAMESPACES'));

        collect($secretNamespaces)->filter()->each(function($secretNamespace){
            $credentials = Credentials::getInstance();
            @unlink($credentials->getCachedSecretPath($credentials->getCacheKey($secretNamespace)));
        });

        $this->info('Successfully refreshed credentials.');
    }
}