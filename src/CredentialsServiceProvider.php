<?php

namespace SecretsManager\Credentials;

use Illuminate\Support\ServiceProvider;

class CredentialsServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([RefreshCredentialsCommands::class]);
    }
}