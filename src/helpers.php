<?php
use SecretsManager\Credentials\Credentials;

if (! function_exists('credentials')) {
    /**
     * Get a value from any of the specified namespaces.
     *
     * @param string $key
     * @param null $default
     * @return mixed
     */
    function credentials(string $key = null, $default = null)
    {
        $credentials = Credentials::getInstance();

        if(is_null($key)) {
            return $credentials;
        }

        return $credentials->get($key, $default);
    }
}